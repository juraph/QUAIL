#include "quail/quail_base.hpp"

void int_handler(int dum) {

  rclcpp::shutdown();

  exit(0);
}

// Entry point for QUAIL
int main(int argc, char *argv[]) {
  // force flush of the stdout buffer.
  // this ensures a correct sync of all prints
  // even when executed simultaneously within the launch file.
  setvbuf(stdout, NULL, _IONBF, BUFSIZ);
  // Set up handle to handle ctrl+c killing of software
  signal(SIGINT, int_handler);
  rclcpp::init(argc, argv);

  rclcpp::executors::SingleThreadedExecutor exe;
  rclcpp::NodeOptions controller_options;

  controller_ = std::make_shared<quail::Controller>(
      "controller", std::move(controller_options));

  exe.add_node(controller_->get_node_base_interface());

  if (controller_->configure().id() !=
      lifecycle_msgs::msg::State::PRIMARY_STATE_INACTIVE) {
    throw std::runtime_error("Could not configure the controller!");
  }
  if (controller_->activate().id() !=
      lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE) {
    throw std::runtime_error("Could not activate the controller!");
  }

  exe.spin();

  rclcpp::shutdown();

  return 0;
}
