#ifndef __QUAIL_BASE_H_
#define __QUAIL_BASE_H_

#include <iostream>
#include <memory>
#include <signal.h>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <lifecycle_msgs/msg/state.hpp>
#include <rclcpp/rclcpp.hpp>

#include "quail_control/controller_node.hpp"

std::shared_ptr<quail::Controller> controller_;

/*

 Entry point for QUAIL

*/
int main(int argc, char *argv[]);

/// Handler for ctrl+c interrupts
void int_handler(int dum);

#endif // __QUAIL_BASE_H_
