#! /bin/sh
cd ../
rm -r build/*
rm -r install/*
colcon build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON --symlink-install
