#ifndef QUAIL_CONTROLLER_NODE__QUAIL_CONTROLLER_NODE_HPP_
#define QUAIL_CONTROLLER_NODE__QUAIL_CONTROLLER_NODE_HPP_

#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <utility>

#include "lifecycle_msgs/msg/transition.hpp"

#include "rclcpp/publisher.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_components/register_node_macro.hpp"
#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "rclcpp_lifecycle/lifecycle_publisher.hpp"

#include "rcutils/logging_macros.h"
#include "std_msgs/msg/string.hpp"

#include "quail_control/motor_driver_node.hpp"
#include "quail_control/sensor_node.hpp"
#include "quail_msgs/msg/bar_state.hpp"

#include <pigpiod_if2.h>

#define ENCODER_PIN 21

using namespace std::chrono_literals;
using namespace rclcpp_lifecycle::node_interfaces;

namespace quail {

class Controller : public rclcpp_lifecycle::LifecycleNode {

public:
  explicit Controller(const rclcpp::NodeOptions &options)
      : rclcpp_lifecycle::LifecycleNode("Controller", options), p_gain_1_(0),
        p_gain_2_(0) {}

  Controller(const std::string &node_name, const rclcpp::NodeOptions &options);

  LifecycleNodeInterface::CallbackReturn
  on_configure(const rclcpp_lifecycle::State &);

  LifecycleNodeInterface::CallbackReturn
  on_activate(const rclcpp_lifecycle::State &);

  LifecycleNodeInterface::CallbackReturn
  on_deactivate(const rclcpp_lifecycle::State &);

  LifecycleNodeInterface::CallbackReturn
  on_cleanup(const rclcpp_lifecycle::State &);

  LifecycleNodeInterface::CallbackReturn
  on_shutdown(const rclcpp_lifecycle::State &state);

private:
  /// Callback for the sensor bar topic
  void
  sensor_callback(const quail_msgs::msg::BarState::SharedPtr bar_readings_msg);

  // Control loop
  void control_loop();

  // Motor driver node object
  MotorDriver motor_driver_;

  // Sensor node object
  std::shared_ptr<SensorNode> sensor_node_;

  // Subscriber to the sensor node
  rclcpp::Subscription<quail_msgs::msg::BarState>::SharedPtr sensor_sub_;

  quail_msgs::msg::BarState current_bar_reading_;

  // Variable returned by pigpio_start()
  int pi_;

  // Timer for executing the control loop
  rclcpp::TimerBase::SharedPtr timer_;

  // Control gain values
  // Gain for case two sensors detect line (Slight turn)
  const float p_gain_1_;
  // Gain for case one sensor detects line (Sharp turn)
  const float p_gain_2_;
  // Straight forward cose for bar readings
  std::array<int8_t, 3> straight_ahead_arr_;

  // Slight turn cases
  std::array<int8_t, 3> slight_left_arr_;
  std::array<int8_t, 3> slight_right_arr_;

  std::array<int8_t, 3> hard_left_arr_;
  std::array<int8_t, 3> hard_right_arr_;
};
} // namespace quail

#endif // QUAIL_CONTROLLER_NODE__QUAIL_CONTROLLER_NODE_HPP_
