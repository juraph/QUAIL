#ifndef SENSOR_NODE_H_
#define SENSOR_NODE_H_

#include <chrono>
#include <functional>
#include <memory>
#include <mutex>
#include <pigpiod_if2.h>
#include <string>

#include "quail_msgs/msg/bar_state.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

namespace quail {
class SensorNode : public rclcpp::Node {
public:
  explicit SensorNode(const rclcpp::NodeOptions &options)
      : Node("sensor_node", options), sensor_pin_1_(16), sensor_pin_2_(20),
        sensor_pin_3_(21) {}

  // Constructor
  SensorNode(const std::string &node_name, const rclcpp::NodeOptions &options,
             double pi);

  // Destructor
  ~SensorNode();

private:
  void timer_callback();

  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<quail_msgs::msg::BarState>::SharedPtr publisher_;
  size_t count_;

  // Pins for reading the light bar
  const unsigned sensor_pin_1_;
  const unsigned sensor_pin_2_;
  const unsigned sensor_pin_3_;

  // Variable returned by pigpio_start()
  double pi_;
};
} // namespace quail

#endif // SENSOR_NODE_H_
