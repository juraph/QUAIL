#ifndef __QUAIL_MOTOR_DRIVER_NODE_HPP_
#define __QUAIL_MOTOR_DRIVER_NODE_HPP_

#include <bitset>
#include <chrono>
#include <memory>
#include <pigpiod_if2.h>
#include <string>

#include "rclcpp/publisher.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

// Motor pins
#define L_MOTOR_REV 12
#define R_MOTOR_REV 19
#define L_MOTOR_FWD 18
#define R_MOTOR_FWD 13

#define M_FREQ 10
#define M_DUTY_MAX 1000000

using namespace std::chrono_literals;

namespace quail {

class MotorDriver {

public:
  // Constructor
  MotorDriver();
  // Destructor
  ~MotorDriver();

  /*
   * @Brief Initialises the gpio outputs
   */
  bool configure(int pi);

  /*
   * @Brief Writes zero to all of the PWM outputs for shutdown
   */
  void shutdown_driver();

  /*
   * @Brief Send velocity commands directly to motors
   *
   * @Param Unsigned int ranging from -255 -> 255
   */
  void drive_motors_direct(float l_track_pow, float r_track_pow);

private:
  /// Variable returned by pigpio_start()
  double pi_;

  /// Name of our node, Used for logging
  const char *driver_name_ = "MotorDriver";
};
} // namespace quail

#endif // _QUAIL_MOTOR_DRIVER_NODE_HPP_
