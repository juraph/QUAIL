#include "quail_control/sensor_node.hpp"

using namespace std::chrono_literals;

namespace quail {
/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */

SensorNode::SensorNode(const std::string &node_name,
                       const rclcpp::NodeOptions &options, double pi)
    : Node(node_name), sensor_pin_1_(16), sensor_pin_2_(20), sensor_pin_3_(21) {

  // Assign the pigpio_start pi
  pi_ = pi;

  // Create the publisher
  publisher_ =
      this->create_publisher<quail_msgs::msg::BarState>("sensor_bar", 10);
  timer_ = this->create_wall_timer(
      100ms, std::bind(&SensorNode::timer_callback, this));
}

SensorNode::~SensorNode() {}

void SensorNode::timer_callback() {
  const int pin_1_val = gpio_read(pi_, sensor_pin_1_);
  const int pin_2_val = gpio_read(pi_, sensor_pin_2_);
  const int pin_3_val = gpio_read(pi_, sensor_pin_3_);
  auto message = quail_msgs::msg::BarState();
  message.reading.at(0) = pin_1_val;
  message.reading.at(1) = pin_2_val;
  message.reading.at(2) = pin_3_val;
  publisher_->publish(message);
}

} // namespace quail

#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(quail::SensorNode)
