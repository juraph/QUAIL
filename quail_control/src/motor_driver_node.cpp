#include "quail_control/motor_driver_node.hpp"
#include <rcutils/logging_macros.h>

using namespace std::chrono_literals;

namespace quail {

MotorDriver::MotorDriver() : pi_(0) {}

MotorDriver::~MotorDriver() {}

bool MotorDriver::configure(int pi) {
  pi_ = pi;

  // Initialise the GPIO outputs
  if (set_mode(pi_, R_MOTOR_REV, PI_OUTPUT) > 0) {

    RCUTILS_LOG_INFO_NAMED(driver_name_, "Could not initialise GPIO output: %d",
                           R_MOTOR_REV);
    return false;
  }
  if (set_mode(pi_, L_MOTOR_REV, PI_OUTPUT) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Could not initialise GPIO output: %d",
                           L_MOTOR_REV);
    return false;
  }
  if (set_mode(pi_, R_MOTOR_FWD, PI_OUTPUT) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Could not initialise GPIO output: %d",
                           R_MOTOR_FWD);
    return false;
  }
  if (set_mode(pi_, L_MOTOR_FWD, PI_OUTPUT) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Could not initialise GPIO output: %d",
                           L_MOTOR_FWD);
    return false;
  }

  // Test if working by writing 0 to the outputs
  if (hardware_PWM(pi_, R_MOTOR_REV, 0, 0) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Test write to %d failed!",
                           R_MOTOR_REV);
    return false;
  }
  if (hardware_PWM(pi_, L_MOTOR_REV, 0, 0) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Test write to %d failed!",
                           L_MOTOR_REV);
    return false;
  }
  if (hardware_PWM(pi_, R_MOTOR_FWD, 0, 0) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Test write to %d failed!",
                           R_MOTOR_FWD);
    return false;
  }
  if (hardware_PWM(pi_, L_MOTOR_FWD, 0, 0) > 0) {
    RCUTILS_LOG_INFO_NAMED(driver_name_, "Test write to %d failed!",
                           L_MOTOR_FWD);
    return false;
  }

  return true;
}

void MotorDriver::shutdown_driver() {

  RCUTILS_LOG_INFO_NAMED(driver_name_, "Shutting down");
  // Make sure we aren't driving the motors when shutdown is called
  set_PWM_dutycycle(pi_, L_MOTOR_REV, 0);
  set_PWM_dutycycle(pi_, L_MOTOR_FWD, 0);
  set_PWM_dutycycle(pi_, R_MOTOR_REV, 0);
  set_PWM_dutycycle(pi_, R_MOTOR_FWD, 0);
}

void MotorDriver::drive_motors_direct(float l_track_pow, float r_track_pow) {
  int ret_l = 0;
  int ret_r = 0;

  // Reset pwm currently being written to GPIO.
  set_PWM_dutycycle(pi_, L_MOTOR_REV, 0);
  set_PWM_dutycycle(pi_, L_MOTOR_FWD, 0);
  set_PWM_dutycycle(pi_, R_MOTOR_REV, 0);
  set_PWM_dutycycle(pi_, R_MOTOR_FWD, 0);

  if (l_track_pow != 0) {
    ret_l = (l_track_pow > 0)
                ? hardware_PWM(pi_, L_MOTOR_FWD, M_FREQ,
                               int(l_track_pow * M_DUTY_MAX))
                : hardware_PWM(pi_, L_MOTOR_REV, M_FREQ,
                               int(abs(l_track_pow * M_DUTY_MAX)));
  }
  if (r_track_pow != 0) {
    ret_r = (r_track_pow > 0)
                ? hardware_PWM(pi_, R_MOTOR_FWD, M_FREQ,
                               int(r_track_pow * M_DUTY_MAX))
                : hardware_PWM(pi_, R_MOTOR_REV, M_FREQ,
                               int(abs(r_track_pow * M_DUTY_MAX)));
  }

  if (ret_l != 0 || ret_r != 0) {
    RCUTILS_LOG_ERROR_NAMED(
        driver_name_, "Could not drive motors, status left: %d, right: %d",
        ret_l, ret_r);
  }
}

} // namespace quail
