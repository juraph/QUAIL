#include "quail_control/controller_node.hpp"

#include "quail_msgs/msg/bar_state.hpp"

using namespace std::chrono_literals;

namespace quail {

Controller::Controller(const std::string &node_name,
                       const rclcpp::NodeOptions &options =
                           rclcpp::NodeOptions().use_intra_process_comms(false))
    : rclcpp_lifecycle::LifecycleNode(node_name, options), p_gain_1_(0.2),
      p_gain_2_(0.5) {}

LifecycleNodeInterface::CallbackReturn
Controller::on_configure(const rclcpp_lifecycle::State &) {

  RCUTILS_LOG_INFO_NAMED(get_name(), "on_configure() is called.");

  pi_ = pigpio_start(NULL, NULL);

  if (pi_ < 0) {
    RCUTILS_LOG_ERROR_NAMED(
        get_name(),
        "Could not initialise the GPIO output, have you ran `sudo pigpiod?`");
    return LifecycleNodeInterface::CallbackReturn::ERROR;
  }
  // Configure the motor driver
  motor_driver_.configure(pi_);

  // Initialise the sensor node
  rclcpp::NodeOptions sensor_options;
  sensor_node_ =
      std::make_shared<SensorNode>("sensor_node", sensor_options, pi_);

  std::thread sensor_thread(
      static_cast<void (*)(rclcpp::Node::SharedPtr)>(rclcpp::spin),
      sensor_node_);

  // setting threads priority is not mandatory
  sched_param sch;
  int policy;
  pthread_getschedparam(sensor_thread.native_handle(), &policy, &sch);
  sch.sched_priority = 10;
  pthread_setschedparam(sensor_thread.native_handle(), SCHED_FIFO, &sch);

  sensor_thread.detach();

  // Subscribe to the sensor bar messages
  sensor_sub_ = this->create_subscription<quail_msgs::msg::BarState>(
      "sensor_bar", 10,
      std::bind(&Controller::sensor_callback, this, std::placeholders::_1));
  RCUTILS_LOG_INFO_NAMED(get_name(), "Created callback");

  // Initialise the turn case vectors
  straight_ahead_arr_ = {0, 1, 0};
  slight_left_arr_ = {1, 1, 0};
  slight_right_arr_ = {0, 1, 1};
  hard_left_arr_ = {1, 0, 0};
  hard_right_arr_ = {0, 0, 1};

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::
      LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Controller::on_activate(const rclcpp_lifecycle::State &) {

  RCUTILS_LOG_INFO_NAMED(get_name(), "on_activate() is called.");

  // Start the control loop
  timer_ = this->create_wall_timer(100ms,
                                   std::bind(&Controller::control_loop, this));

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::
      LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Controller::on_deactivate(const rclcpp_lifecycle::State &) {

  RCUTILS_LOG_INFO_NAMED(get_name(), "on_deactivate() is called.");

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::
      LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Controller::on_cleanup(const rclcpp_lifecycle::State &) {

  RCUTILS_LOG_INFO_NAMED(get_name(), "on cleanup is called.");

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::
      LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Controller::on_shutdown(const rclcpp_lifecycle::State &state) {

  RCUTILS_LOG_INFO_NAMED(get_name(), "on shutdown is called from state %s.",
                         state.label().c_str());
  motor_driver_.shutdown_driver();

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::
      LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

void Controller::sensor_callback(
    const quail_msgs::msg::BarState::SharedPtr bar_readings_msg) {
  current_bar_reading_ = *bar_readings_msg;
}

void Controller::control_loop() {
  // Read the sensor bar
  RCUTILS_LOG_DEBUG_NAMED(get_name(), "Control loop readings: [%d, %d, %d]",
                          current_bar_reading_.reading.at(0),
                          current_bar_reading_.reading.at(1),
                          current_bar_reading_.reading.at(2));

  // [o x o] Straight forward case
  if (current_bar_reading_.reading == straight_ahead_arr_) {
    motor_driver_.drive_motors_direct(1, 1);
  }
  // [x x o] / [o x x] Slight turn case
  else if (current_bar_reading_.reading == slight_left_arr_) {
    motor_driver_.drive_motors_direct(1 - p_gain_1_, 1);
  } else if (current_bar_reading_.reading == slight_right_arr_) {
    motor_driver_.drive_motors_direct(1, 1 - p_gain_1_);
  }
  // [x o o ] / [o o x] Hard turn case
  else if (current_bar_reading_.reading == hard_left_arr_) {
    motor_driver_.drive_motors_direct(1 - p_gain_2_, 1);
  } else if (current_bar_reading_.reading == hard_right_arr_) {
    motor_driver_.drive_motors_direct(1, 1 - p_gain_2_);
  }
  // [o o o] / [x x x] Panic case
  else {
    motor_driver_.drive_motors_direct(0, 0);
    RCUTILS_LOG_ERROR_NAMED(get_name(), "Lost the line!!");
  }
} // namespace quail

} // namespace quail

RCLCPP_COMPONENTS_REGISTER_NODE(quail::Controller)
